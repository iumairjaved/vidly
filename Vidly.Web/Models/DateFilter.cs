﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Vidly.Web.Models
{
    public class DateFilter : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var customer = (Customer)validationContext.ObjectInstance;

            if (customer.DateOfBirth == null) {
                return new ValidationResult("Date is required");
            }
            else if (customer.DateOfBirth.Length == 8) {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult("Please insert Date correctly. *DD/MM/YY");
            }
        }
    }
}