namespace Vidly.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'54bbd99c-ebdf-48ea-b434-b4c27793af11', N'admin@vidly.com', 0, N'AC4UYAsWSSsHNp6EhAt/PCPMjUjB6rTygZhVK/0zSXdJWv9FcMoDsfWqSLtBcLmhQg==', N'4f15f4a7-ff21-4e14-a46c-c534f08ac65b', NULL, 0, 0, NULL, 1, 0, N'admin@vidly.com')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'92ffeab4-1f17-4349-b856-f756087d9b38', N'guest@vidly.com', 0, N'AMcAlYBbSllQbzDf3bpX44WFpdk/VCp8MV2tT3RFFY4CZDK4KW5mORt3aa+vKr9WIA==', N'5ed9fcca-d1e1-470d-b82c-52cc292bca5f', NULL, 0, 0, NULL, 1, 0, N'guest@vidly.com')

INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'dac2f240-f5fb-431e-a09c-12489cd0e596', N'canManageMovies')
INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'54bbd99c-ebdf-48ea-b434-b4c27793af11', N'dac2f240-f5fb-431e-a09c-12489cd0e596')
");
        }
        
        public override void Down()
        {
        }
    }
}
