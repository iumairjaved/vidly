namespace Vidly.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateCustomersTable : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO CUSTOMERS(Name,IsSubscribedToNewsLetter,MembershipTypeId) VALUES('Umair',1,2)");
            Sql("INSERT INTO CUSTOMERS(Name,IsSubscribedToNewsLetter,MembershipTypeId) VALUES('Danish',0,1)");
            Sql("INSERT INTO CUSTOMERS(Name,IsSubscribedToNewsLetter,MembershipTypeId) VALUES('Hammad',0,4)");
            Sql("INSERT INTO CUSTOMERS(Name,IsSubscribedToNewsLetter,MembershipTypeId) VALUES('Sohaib',1,3)");
        }
        
        public override void Down()
        {
        }
    }
}
