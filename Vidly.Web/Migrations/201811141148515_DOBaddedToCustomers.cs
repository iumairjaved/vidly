namespace Vidly.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DOBaddedToCustomers : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "DateOfBirth", c => c.String(maxLength: 8));
        }
        
        public override void Down()
        {
            
        }
    }
}
