namespace Vidly.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DatesAddedtoMovies : DbMigration
    {
        public override void Up()
        {
           
            AddColumn("dbo.Movies", "DateAdded", c => c.String(maxLength: 10));
            AddColumn("dbo.Movies", "ReleasedDate", c => c.String(maxLength: 10));
          
        }
        
        public override void Down()
        {
        
            DropColumn("dbo.Movies", "ReleasedDate");
            DropColumn("dbo.Movies", "DateAdded");
         
        }
    }
}
