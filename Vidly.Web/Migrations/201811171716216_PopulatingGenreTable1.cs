namespace Vidly.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulatingGenreTable1 : DbMigration
    {
        public override void Up()
        {
            Sql("Insert into Genres values(1,'Action')");
            Sql("Insert into Genres values(2,'Romance')");
            Sql("Insert into Genres values(3,'Comedy')");
            Sql("Insert into Genres values(4,'Animated')");
            Sql("Insert into Genres values(5,'Horror')");
        }
        
        public override void Down()
        {
        }
    }
}
