namespace Vidly.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateMigrationTypeNameProperty : DbMigration
    {
        public override void Up()
        {
            Sql("Update MembershipTypes set Name = 'Free To Go' where ID = 1");
            Sql("Update MembershipTypes set Name = 'Premium-1Month' where ID = 2");
            Sql("Update MembershipTypes set Name = 'Gold-3Months' where ID = 3");
            Sql("Update MembershipTypes set Name = 'Platinum-12Months' where ID = 4");
        }
        
        public override void Down()
        {
        }
    }
}
