namespace Vidly.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulatingGenreTable : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO GENRES VALUES(1,'Comedy')");
            Sql("INSERT INTO GENRES VALUES(2,'Action')");
            Sql("INSERT INTO GENRES VALUES(3,'Family')");
            Sql("INSERT INTO GENRES VALUES(4,'Romance')");
            Sql("INSERT INTO GENRES VALUES(5,'Animated')");
        }
        
        public override void Down()
        {
        }
    }
}
