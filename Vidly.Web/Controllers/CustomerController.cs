﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vidly.Web.Models;
using Vidly.Web.ViewModels;
using System.Web.Http;
using System.Net;

namespace Vidly.Web.Controllers
{
    public class CustomerController : Controller
    {
        private ApplicationDbContext _context;

        public CustomerController() {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        public ActionResult CustomerForm()
        {
            var viewModel = new NewCustomerViewModel
            {
                MembershipTypes = _context.MembershipTypes.ToList(),
                Customer = new Customer()
         
            };
            return View("CustomerForm", viewModel);
        }
        [System.Web.Http.HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Customer customer)
        {
            if (!ModelState.IsValid) {
                NewCustomerViewModel viewModel = new NewCustomerViewModel
                {
                    Customer = customer,
                    MembershipTypes = _context.MembershipTypes.ToList()
                };
                return View("CustomerForm", viewModel);
            }

            if (customer.ID == 0)
                 { _context.Customers.Add(customer);
            }
            else {
                var customerinDB = _context.Customers.Single(c => c.ID == customer.ID);
                customerinDB.Name = customer.Name;
                customerinDB.DateOfBirth = customer.DateOfBirth;
                customerinDB.MembershipTypeId = customer.MembershipTypeId;
                customerinDB.IsSubscribedToNewsLetter = customer.IsSubscribedToNewsLetter;
            }
                _context.SaveChanges();

            return RedirectToAction("Index","Customer");
        }

        public ActionResult Edit(int Id)
        {
            var customer = _context.Customers.SingleOrDefault(i => i.ID == Id);
            if (customer==null)
                return HttpNotFound();

            var viewModel = new NewCustomerViewModel
            {
                Customer = customer,
                MembershipTypes = _context.MembershipTypes.ToList()
            };

            return View("CustomerForm", viewModel);
        }
        public ActionResult Index()
        {

            var customers = _context.Customers.Include(c=>c.MembershipType).ToList();
            return View(customers);
        }

        public ActionResult Details(int Id)
        {

            var customer = _context.Customers.Include(c=>c.MembershipType).SingleOrDefault(c=>c.ID==Id);
            return View(customer);
        }
        public void Delete(int Id)
        {

            var customer = _context.Customers.SingleOrDefault(c => c.ID == Id);
            if(customer == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            else {
                _context.Customers.Remove(customer);
                _context.SaveChanges();
            }
        }
     }
    }