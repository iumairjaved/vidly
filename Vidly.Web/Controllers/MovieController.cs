﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vidly.Web.Models;
using System.Data.Entity;
using Vidly.Web.ViewModels;

namespace Vidly.Web.Controllers
{
    public class MovieController : Controller
    {
        private ApplicationDbContext _context;
        public MovieController()
        {
            _context = new ApplicationDbContext();
        }


        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        public ActionResult Index()
        {
            var movieList = _context.Movies.Include(m => m.Genre).ToList();
            return View(movieList);
        }

        public ActionResult MovieForm()
        {
            NewMovieViewModel viewModel = new NewMovieViewModel
            {
                Genre = _context.Genres.ToList(),
                Movie = new Movie()
            };
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Movie movie)
        {
            
            //if (ModelState.IsValid == false) {

            //    NewMovieViewModel viewModel = new NewMovieViewModel {
            //        Genre = _context.Genres.ToList(),
            //        Movie = movie
            //    };

            //    return View("MovieForm",viewModel);
            //}


            if (movie.ID == 0)
            {
                _context.Movies.Add(movie);
            }
            else
            {
                var movieInDB = _context.Movies.Single(m => m.ID == movie.ID);
                movieInDB.Name = movie.Name;
                movieInDB.NumberInStock = movie.NumberInStock;
                movieInDB.ReleasedDate = movie.ReleasedDate;
                movieInDB.DateAdded = movie.DateAdded;
                movieInDB.GenreId = movie.GenreId;
            }
            _context.SaveChanges();
            return RedirectToAction("Index", "Movie");
        }

        public ActionResult Details(int Id)
        {

            var movie = _context.Movies.Include(m => m.Genre).SingleOrDefault(m => m.ID == Id);
            return View(movie);
        }

        public ActionResult Edit(int Id)
        {

            var movie = _context.Movies.SingleOrDefault(m => m.ID == Id);
            if (movie == null)
            {
                HttpNotFound();
            }

            var viewModel = new NewMovieViewModel
            {
                Movie = movie,
                Genre = _context.Genres.ToList()
            };

            return View("MovieForm", viewModel);



        }
    }
}